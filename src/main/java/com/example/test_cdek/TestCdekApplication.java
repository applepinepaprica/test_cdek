package com.example.test_cdek;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCdekApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCdekApplication.class, args);
	}
}
